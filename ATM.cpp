#include<iostream>
#include<fstream>
#include <stdexcept>
#include <string>
#include <windows.h>
#include<cstdlib>
using namespace std;

string getpassword( const string&);
void deposit(); 				// to deposit money in the account
void addrecord();				// to add new account only by admin
void delrecord();				// to delete an account only by admin
void disprecord();				// displays all the details on an account holder
void dispall();					// dispalys all the accounts only to admin
void withdraw();				// withdraws the amount from an account
void admin();					// admin access admin account
void custm();					// for customers to access
float w,z;
const string pass= "a1b2";

class account{

	int accountno, cls;
    char name[30], type;
	double deposit, withdraw;
	string pin;

  public:

	account():deposit(0),withdraw(0){}		// constructor to initailaize amount

	int giveaccountno(){

		return accountno;
    }

    string giveaccountpin(){

        return pin;
    }

	void getdata(int mrno){

		accountno = mrno;

		cout<<"ACCOUNT NUMBER        ::  ";
		cin>>accountno;

		pin = getpassword("ENTER PIN");

		cout<<"ENTER YOUR NAME       ::  ";
		cin.getline(name,30,'.');

		cout<<"ENTER TYPE OF ACCOUNT SAVING(s)/CURRENT(c)  ::  ";
		cin>>type;

        cout<<"ENTER INITIAL AMOUNT  ::Rs  ";
        cin>>deposit;

	}

	void withdrawal(int m) {

		cout<<"AMOUNT BEFORE WITHDRAWING::Rs  "<<deposit<<endl;

		if(m > deposit){

			cout<<"NOT ENOUGH AMOUNT IN THE ACCOUNT"<<endl;

		}

		else {

			deposit -= m;
			cout<<"CURRENT BALANCE ::Rs  "<<deposit<<endl;

		}
	}


	void deposital(int m) {

		cout<<"AMOUNT BEFORE DEPOSIT::Rs.  "<<deposit<<endl;

		deposit += m;

		cout<<"CURRENT BALANCE :: Rs. "<<deposit<<endl;

  }

	void dispdata() {

		int scrnt=0;

		if(scrnt==1){

			system("cls");
			cout<<"DISPLAY ALL MENU "<<endl;
			scrnt=0;
		}

        cout<<"ACCOUNT NUMBER              :: "<<accountno<<endl;
		cout<<" NAME OF DEPOSITER             :: "<<name<<endl;
		cout<<" TYPE OF ACCOUNT SAVING(s)/CURRENT(c)  :: "<<type<<endl;
        cout<<"BALANCE  ::Rs  "<<deposit<<endl;

		scrnt++;
     }

};

int main(){

	int op;

    do{
        system("cls");
		cout<<"\t\t\t\t WELCOME TO ATM "<<endl;
		cout<<"\t1. ADMINISTRATOR \n\n\t2. CUSTOMER\n\n\t3. EXIT\n";
		cout<<"\tSelect an option"<<endl;
		cin>>op;

        for(int i= 0; i<40; i++)
			cout<<"--";

		cout<<endl;
        switch(op){

            case 1: admin();break;
            case 2: custm();break;
        }

    }
    while(op!=3);
    return 0;
}

void addrecord() {

	account obj_1,obj_2;
	ifstream fin;
	fin.open("banking.txt",ios::binary | ios::in);

	int recsize = sizeof(account);

	fin.seekg(0,ios::end);
    fin.seekg(recsize,ios::cur);
    fin>>(char*)&obj_1;

	int mrno = obj_1.giveaccountno();
    fin.close();

    system("cls");
    ofstream fout;

    cout<<"\t\t\t ADD MENU "<<endl;
    for(int i= 0; i<40; i++)
			cout<<"--";
	cout<<endl;

    obj_2.getdata(mrno);
	fout.open("banking.txt",ios::binary |ios::app);

    fout.write((char*)&obj_2,recsize);
    cout<<"RECORD ADDED TO DATABASE "<<"  Press any key to continue... "<<endl;

	fout.close();

}


void dispall(){

	account obj_3;
    ifstream fin;
    int recsize = sizeof(account);
	int countrec = 0;

	system("cls");

    cout<<"\t\t\tDISPLAY THE LOGS "<<endl;
    for(int i= 0; i<40; i++)
			cout<<"--";
	cout<<endl;

    fin.open("banking.txt",ios::in);

	while(fin.read((char*)&obj_3,recsize)) {

		obj_3.dispdata();
		countrec++;
        cout<<endl<<"PRESS ANY KEY FOR NEXT...."<<endl;

	}

	cout<<" END OF FILE.TOTAL NUMBER OF LOGS..."<<countrec<<endl;

    fin.close();
}

void disprecord(){

	account obj_4;
	ifstream fin;

    int mrno,flag=0;
    int recsize=sizeof(account);

	system("cls");

    cout<<"\t\t\tDISPLAY A RECORD MENU"<<endl;
    for(int i= 0; i<40; i++)
			cout<<"--";
	cout<<endl;

    fin.open("banking.txt",ios::in);

	cout<<" ENTER  THE ACCOUNT NUMBER  "<<endl;
	cin>>mrno;

    while(fin.read((char*)&obj_4,recsize)){

		if (obj_4.giveaccountno()==mrno){

			obj_4.dispdata();
			cout<<"Press any key....."<<endl;
			flag=1;
			break;
		}

	}

	if(flag==0){

		cout<<"NO SUCH ACCOUNT EXIST "<<endl;
		cout<<"Press any key......"<<endl;
	}

    fin.close();
}

void delrecord(){

	account obj_5;
    ifstream fin;
    ofstream temp;
    int mrno,flag;
    int recsize=sizeof(account);


	cout<<"\t\t\tMODIFYING DATABASE"<<endl;
	for(int i= 0; i<40; i++)
			cout<<"--";

	cout<<endl;
    fin.open("banking.txt",ios::in);

   	temp.open("temp.txt",ios::app);

    if(!temp) {
		cout<<"FILE OPEN ERROR"<<endl;

	}

	cout<<" ENTER THE ACCOUNT NUMBER "<<endl;
	cin>>mrno;

	while(fin.read((char*)&obj_5,recsize)){

		if(obj_5.giveaccountno()==mrno){

			obj_5.dispdata();
			char confirm;
			cout<<"ARE YOU SURE TO SAVE CHANGES (Y/N)..."<<endl;
			cin>>confirm;

			if(confirm=='Y'||confirm=='y'){

				fin.read((char*)&obj_5,recsize);

				cout<<"RECORD DELETED FORM DATABASE"<<endl;
				cout<<"press any key..."<<endl;
				flag=1;

            }

			if(flag ==1)
                break;


		}

		temp.write((char*)&obj_5,recsize);
	}
		fin.close();
		temp.close();

		remove("banking.txt");
		rename("temp.txt","banking.txt");

		if(flag==0){

	    cout<<"NO SUCH ACCOUNT EXIST"<<endl;
		cout<<"Press any key....."<<endl;
	}

}

void withdraw(){

	account obj_9;
	ifstream fin;
	ofstream fout;
	int mrno=0;
    int recsize=sizeof(account);
	system("cls");

	cout<<"\t\t\tWITHDRAWAL MENU"<<endl;
	for(int i= 0; i<40; i++)
			cout<<"--";

	cout<<endl;

	fin.open("banking.txt",ios::in);
	fout.open("banking.txt",ios::app);

	fin.seekg(ios::beg);
	cout<<"ENTER ACCOUNT NUMBER"<<endl;
    cin>>mrno;
    int check;

	while(fin.read((char*)&obj_9,recsize)){
        check = 0;
		if(obj_9.giveaccountno()==mrno){
            check = 1;
			cout<<"ENTER THE AMOUNT TO BE WITHDRAW::Rs ";
			cin>>w;

			obj_9.withdrawal(w);
			fin.seekg(recsize,ios::cur);
			fout.write((char*)&obj_9,recsize);
		}
		if(check==1)
            break;
	}

	 fout.close();
	 fin.close();


}

void deposit(void){

	account obj_10;

	ifstream fin;
	ofstream fout;

	int mrno=0;
    int recsize=sizeof(account);
    system("cls");

	cout<<"\t\t\tDEPOSITAL MENU"<<endl;
	for(int i= 0; i<40; i++)
			cout<<"--";

	cout<<endl;

	fin.open("banking.txt",ios::in);
	fout.open("banking.txt",ios::app);

    fin.seekg(recsize,ios::beg);
	cout<<"ENTER ACCOUNT NUMBER"<<endl;
	cin>>mrno;
    int check;
	while(fin.read((char*)&obj_10,recsize)){
        check = 0;
		if(obj_10.giveaccountno()==mrno){

            check =1;
			cout<<"ENTER THE AMOUNT TO BE DEPOSITED ::Rs ";
			cin>>w;

			obj_10.deposital(w);
			//
			fin.seekg(recsize,ios::cur);
			fout.write((char*)&obj_10,recsize);
		}
		if(check==1)
            break;
    }

    fout.close();
    fin.close();

}
void admin(){
 system("cls");
int menuch, check = 1;
while(check ==1){
    string p = getpassword("ENTER PIN");
    if(p == pass){
        check = 0;
    do{
        cout<<"\t\t\t 1.NEW ACCOUNT"<<endl;
        cout<<endl<<"\t\t\t 2.CLOSE AN ACCOUNT"<<endl;
        cout<<endl<<"\t\t\t 3.DISPLAY ALL LOGS"<<endl;
        cout<<endl<<"\t\t\t\t4.EXIT "<<endl;
        cout<<endl<<"ENTER YOUR CHOICE"<<endl;
        cin>>menuch;

		switch(menuch){

			case 1:addrecord();break;
			case 2:system("cls");delrecord();break;
			case 3:dispall();break;
        }
    }while(menuch!=4);
  }
else{
        system("cls");
        check=1;
    }
    continue;
}
}
void custm(){
system("cls");

int op, acc;
string p;
cout<<"ENTER ACCOUNT NUMBER";
cin>>acc;
p=getpassword("ENTER PIN");

account obj;
ifstream f;

f.open("banking.txt",ios::in);

int recsize = sizeof(account);

f.seekg(ios::beg);

while(f.read((char*)&obj,recsize)){

	if(!(obj.giveaccountpin()==p)&&(obj.giveaccountno()==acc)){

    do {

    cout<<"\t\t\t 1.DISPLAY THE DETAILS"<<endl;
    cout<<endl<<"\t\t\t 2.WITHDRAW"<<endl;
    cout<<endl<<"\t\t\t 3.DEPOSIT"<<endl;
    cout<<endl<<"\t\t\t\t4.EXIT "<<endl;
    cout<<endl<<"ENTER YOUR CHOICE"<<endl;
    cin>>op;

    switch(op){
        case 1:disprecord();break;
        case 2:withdraw();break;
        case 3:deposit();break;

        }
    }
    while(op!=4);
    }
  }
  f.close();
}
string getpassword( const string& prompt = "ENTER PIN" )
  {
  string result;

  // Set the console mode to no-echo, not-line-buffered input
  DWORD mode, count;
  HANDLE ih = GetStdHandle( STD_INPUT_HANDLE  );
  HANDLE oh = GetStdHandle( STD_OUTPUT_HANDLE );
  if (!GetConsoleMode( ih, &mode ))
    throw runtime_error(
      "getpassword: You must be connected to a console to use this program.\n"
      );
  SetConsoleMode( ih, mode & ~(ENABLE_ECHO_INPUT | ENABLE_LINE_INPUT) );

  // Get the password string
  WriteConsoleA( oh, prompt.c_str(), prompt.length(), &count, NULL );
  char c;
  while (ReadConsoleA( ih, &c, 1, &count, NULL) && (c != '\r') && (c != '\n'))
    {
    if (c == '\b')
      {
      if (result.length())
        {
        WriteConsoleA( oh, "\b \b", 3, &count, NULL );
        result.erase( result.end() -1 );
        }
      }
    else
      {
      WriteConsoleA( oh, "*", 1, &count, NULL );
      result.push_back( c );
      }
    }

  // Restore the console mode
  SetConsoleMode( ih, mode );
  cout<<endl;

  return result;
}


